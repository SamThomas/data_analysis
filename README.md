**********************************************
# Data analysis program using Java and JFreeChart
******************************************
![Chart](http://icons.iconarchive.com/icons/double-j-design/ravenna-3d/256/Pie-Chart-icon.png "Example of 3D chart") 
## What is JFreeChart?

JFreeChart is a free 100% Java chart library that makes it easy for developers to display professional quality charts in their applications. JFreeChart's extensive feature set includes:

* a consistent and well-documented API, supporting a wide range of chart types
* a flexible design that is easy to extend, and targets both server-side and client-side applications
support for many output types, including Swing components, image files (including PNG and JPEG), and vector graphics file formats (including PDF, EPS and SVG);
* JFreeChart is "open source" or, more specifically, free software. 

It is distributed under the terms of the GNU Lesser General Public Licence (LGPL), which permits use in proprietary applications.
